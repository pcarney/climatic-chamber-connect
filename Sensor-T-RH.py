# -*- coding: utf-8 -*-

import serial
import time
import datetime
import math
import logging
import logging.config
import os,traceback,sys

from optparse import OptionParser

from settings import db_url, db_org, db_token, db_bucket, so_IP, so_port

from influxdb_client import InfluxDBClient, Point, WritePrecision

path_to_file = "/etc/influxdb/config.toml"

if __name__ == '__main__':
   p = OptionParser(usage="usage: %prog [options] <serial number file>, ...", version="1.1")

   p.add_option( '--log_file',
       type    = 'string',
       default = '',
       dest    = 'log',
       metavar = 'STR',
       help    = 'Data Log File')

   p.add_option( '--error_file',
       type    = 'string',
       default = '',
       dest    = 'err',
       metavar = 'STR',
       help    = 'Error Log File')

   (opt, args) = p.parse_args()

logging.basicConfig(filename=opt.log, format='%(message)s', level=logging.INFO)
logger = logging.getLogger('__main__')
logging.info('Recording temperature and humidity: press CTRL+C to stop')

#Configure serial port (Windows = COM7, Linux = /dev/ttyACM0)
ser = serial.Serial('/dev/ttyACM0', 9600)
#ser = serial.Serial('COM7', 9600)

#Setup Influxdb client
db_client = InfluxDBClient(url=db_url, token=db_token, org=db_org)

#Initialization of variables
previousT = 20.0

try:
   #Sleep 5 seconds
   time.sleep(5)

   while True:
      if ser.in_waiting > 0:
         line = ser.readline().decode().rstrip()
         #print(line)

         tem, hum = line.split()
         tem = float(tem)
         hum = float(hum)
         #tem,hum=np.fromstring(line, dtype=float ,sep=' ')

         # Formula: http://irtfweb.ifa.hawaii.edu/~tcs3/tcs3/Misc/Dewpoint_Calculation_Humidity_Sensor_E.pdf
         # Range: -45°C < 60°C
         b1=17.62
         c1=243.12
         gam = math.log(hum) + (b1*tem)/(c1+tem);
         dewP = c1*gam/(b1-gam);
         #print("Dew Point:", round(dewP,2))

         #Print the time (int for seconds)
         theTime = int(time.time())
         #print("Time is", theTime)
         date_time = datetime.datetime.fromtimestamp(theTime)
         #print("Converted time is:",date_time)
         
         newT = round(tem,1)
         newRH = round(hum*100,1)
         DP = round(dewP,1)

         #Avoid random values (residual communication by Arduino)
         if newT > 125 or newT < -50:
            newT = previousT

         if newT != previousT:
            previousT = newT
         
            logger = logging.getLogger('__main__')
            logging.info(f'Temp = {newT}°C, RH = {newRH}%, DewPoint = {DP}°C, @ {date_time}')

            point = Point("temperature").field("sensor_T",newT).field("sensor_RH",newRH).field("sensor_DP",dewP)
            db_client.write_api().write(bucket=db_bucket, org=db_org, record=point)

except KeyboardInterrupt:
  logger = logging.getLogger('__main__')
  logging.info('CTL+C Pressed. Closing Connection...')
  db_client.close()
  logging.info('Done')

except Exception:
  logging.basicConfig(filename=opt.err, format='%(asctime)s %(message)s', level=logging.ERROR)
  logger = logging.getLogger('__main__')
  logging.error('Failed for some reason')     
