"""
Module with Command Line Interface for the class ClimaticChamberController.

Name: clim_chamber_cli.py
Author: pszydlik
Date: 13/03/23
"""
import clim_chamber
import logging
import cmd
import pprint
import time

HOST = "192.168.4.199"
PORT = 888

logger = logging.getLogger('clim_chamber')

class ClimaticChamberCLI(cmd.Cmd):
    """Defines a class for the command line interface."""

    prompt = 'climatic chamber control >>> '
    intro = 'Welcome to CLI of Excal Climatic Chamber\nType "help" to get a list of commands.'

    def do_connect(self, args):
        """Brief:  Connect with the TCP server of climatic chamber."""
        self.clim_cham = clim_chamber.ClimaticChamberController(HOST, PORT)
        if self.clim_cham.connect():
            logger.info('Succesfully connected to the Climatic Chamber!')
        else:
            delattr(self, 'clim_cham')

    def do_disconnect(self, args):
        """Brief:  Disconnect from the climatic chamber."""
        if hasattr(self, 'clim_cham'):
            self.clim_cham.client.close()
            delattr(self, 'clim_cham')
            logger.info('Succesfully disconnected from the Climatic Chamber!')
        else:
            logger.warning('!! There was no connection to close !!')

    def do_quit(self, args):
        """Brief:  Quit the CLI."""
        if hasattr(self, 'clim_cham'):
            self.clim_cham.client.close()
            logger.info('Closed the connection with Climatic Chamber.')
        print('Goodbye!')
        return True

    def do_debug(self, args):
        """
        Brief:  Enabling/Disabling debug logger level
        
        Syntax: debug <bool>
        """
        arg_list = args.split(" ")
        if 'false' in arg_list[0]:
            logger.setLevel(logging.INFO)
            clim_chamber.console_handler.setLevel(logging.INFO)
            loggingString = '%(levelname)s:  %(message)s'
            formatter = logging.Formatter(loggingString)
            clim_chamber.console_handler.setFormatter(formatter)
            if clim_chamber.LOG_FILE:
                clim_chamber.file_handler.setFormatter(formatter)
            logger.info('Disabled debug informations.')
        elif 'true' in arg_list[0]:
            logger.setLevel(logging.DEBUG)
            clim_chamber.console_handler.setLevel(logging.DEBUG)
            loggingString = '%(filename)20s:%(lineno)3s --- %(levelname)s:  %(message)s'
            formatter = logging.Formatter(loggingString)
            clim_chamber.console_handler.setFormatter(formatter)
            if clim_chamber.LOG_FILE:
                clim_chamber.file_handler.setFormatter(formatter)
            logger.info('Enabled debug informations.')
        else:
            logger.warning('Invalid option ignored.')


    def do_exec(self, args):
        """Brief:  Execute Python Code (with echo)."""
        try:   # Try executing the code
            print(args)
            response = eval(args)
            logger.info(f'Exec returned: {response}')
        except Exception as e:   # If there is an error, print it out.
            print(e)

    def do_state(self, args):
        """
        Brief: Change state of climatic chamber to one from STATE_DICT.

        Syntax: state <STATE>
        """
        arg_list = args.split(" ")
        if not hasattr(self, 'clim_cham'):
            logger.error("Chamber not connected")
        else:
            response = self.clim_cham.set_state(arg_list[0])
            logger.debug(f'State returned: {response}')
            if not response:
                logger.error("Unsuccesful call")
            else:
                logger.info(f'"{arg_list[0]}" set succesfully.')

    def do_set(self, args):
        """
        Brief:  Change value of variable in climatic chamber.
                Available calls from VAR_DICT.

        Syntax: set <VAR> <VALUE>
        """
        arg_list = args.split(" ")
        if not hasattr(self, 'clim_cham'):
            logger.error("Chamber not connected")
        else:
            response = self.clim_cham.set_value(arg_list[0], arg_list[1])
            logger.debug(f'Set returned: {response}')
            if not response:
                logger.error("Unsuccesful call")
            else:
                logger.info(f'"{arg_list[0]}" with value "{arg_list[1]}" set succesfully.')

    def do_get(self, args):
        """
        Brief:  Change value of variable in climatic chamber.
                Available calls from VAR_DICT.

        Syntax: read <VAR> <VALUE>
        """
        arg_list = args.split(" ")
        if not hasattr(self, 'clim_cham'):
            logger.error("Chamber not connected")
        else:
            response = self.clim_cham.read_value(arg_list[0])
            logger.debug(f'Get returned: {response}')
            if not response:
                logger.error("Unsuccesful call")
            else:
                logger.info(f'"{arg_list[0]}" value is: {response}')

    def do_send(self, args):
        """
        Brief: Send direct text message to the climatic chamber.

        Syntax: send <string>
        """
        arg_list = args.split(" ")
        if not hasattr(self, 'clim_cham'):
            logger.error("Chamber not connected")
        else:
            response = self.clim_cham.client.send_command(arg_list[0])
            logger.debug(f'Send returned: {response}')
            if not response:
                logger.error("Unsuccesful call")
            else:
                logger.info(f'Calling {arg_list[0]} retuned : {response}')

    def do_list_dict(self, args):
        """
        Brief: Display the contents of STATE_DICT and VAR_DICT.

        Syntax: list_dict
        """
        print('\nSTATE_DICT \n')
        pprint.pprint(clim_chamber.STATE_DICT)
        print('\nVAR_DICT \n')
        pprint.pprint(clim_chamber.VAR_DICT)
        print('\n')

    def do_status(self, args):
        """
        Brief:  Updates and display the status of chamber.

        Dictionary containing the value of
            - if_running    - <bool>
            - if_pause      - <bool>
            - if_transition - <bool>
            - if_closed     - <bool>
            - error value   - <str>
        Syntax: status
        """
        response = self.clim_cham.check_status()
        print('\n Chamber Status \n')
        pprint.PrettyPrinter(width=60, compact=False).pprint(response)
        print('\n')
    
    def do_monitor_temp(self, args):
        """
        Brief:  Monitor and log the temperature values with set time period.

        Arg: <time_period> is time in seconds between measurements
        Syntax: monitor_temp <time_period>
        To exit the loop press CTRL+C
        """
        meas_number=0
        arg_list = args.split(" ")
        time_period = int(arg_list[0])
        time_string = time.strftime("%Y-%m-%d_%H-%M-%S")
        fh = open(f'./logs/temperature/measurement_{time_string}.txt', 'w')
        fh.write('Set_point,Control_temp,Temp_air,Probe_1,Probe_2,Dewpoint,Control_humidity,Humidity_sensor\n')
        try:
            while True:
                meas_number += 1
                time.sleep(time_period)
                set_temp = self.clim_cham.read_value('temperature')
                temp_control = self.clim_cham.read_value('temperature_meas')
                temp_probe0 = self.clim_cham.read_value('temperature_air')
                temp_probe1 = self.clim_cham.read_value('temperature_probe')
                temp_probe2 = self.clim_cham.read_value('temperature_probe_2')
                humidity_control = self.clim_cham.read_value('humidity_meas')
                humidity = self.clim_cham.read_value('humidity_sens')
                dewpoint = self.clim_cham.read_value('dewpoint')
                print(f'(CTRL+C to quit) Parameter measurement {meas_number} after {meas_number*time_period} sec.')
                print(f"{'Set point:':<10} {set_temp:<6}")
                print(f"{'Control probe:':<10} {temp_control:<6}")
                print(f"{'Air:':<10} {temp_probe0:<6}")
                print(f"{'Ext probe 1:':<10} {temp_probe1:<6}")
                print(f"{'Ext probe 2:':<10} {temp_probe2:<6}")
                print(f"{'Dewpoint:':<10} {dewpoint:<6}")
                print(f"{'Humidity control:':<10} {humidity_control:<6}")
                print(f"{'Humidity sensor:':<10} {humidity:<6}")
                fh.write(f'{set_temp},{temp_control},{temp_probe0},{temp_probe1},{temp_probe2},{dewpoint},{humidity_control},{humidity}\n')
        except KeyboardInterrupt:
            pass
            fh.close()

            


if __name__ == "__main__":

    ClimaticChamberCLI().cmdloop()
