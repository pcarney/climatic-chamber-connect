# Importing all of our dependencies to collect data from InfluxDB, and run the QTPopup window

import sys
import time
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
import datetime
import pyqtgraph as pg

from matplotlib.animation import FuncAnimation
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from PyQt5.QtWidgets import QApplication
from Ui_progress_dialog import Ui_ProcessMonitoringProgress
from PyQt5 import QtWidgets,QtCore
from PyQt5.QtGui import QPixmap
from datetime import datetime, timedelta
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
from PyQt5.QtWidgets import QDialog
from PyQt5.QtCore import pyqtSignal

from influxdb_client import InfluxDBClient, Point, WritePrecision
from influxdb_client.client.write_api import SYNCHRONOUS

# Connecting our python script to the bucket that is collecting climatic chamber data. 
db_bucket = "THSensor"
db_org = "INFN_CT"
db_token = "a7_QSYYxD5btOOQwecNX3LSspvxUPdsO4wS00hqYiXB69X1QwQXAYSb27GvsSIk1zUWPs38RK1ccyclLIny_1A=="
db_url = "http://teledaq002.cern.ch:8086"

client = InfluxDBClient(url=db_url, token=db_token, org=db_org)

# This is the class that defines all the functionalities of the pop up window. It is called upon when the window refreshes. 
       
class ProgressDialog(QDialog):
    def __init__(self):
        super().__init__()
        
        self.setWindowTitle("Climatic Chamber Data")

        self.progress_bar = QProgressBar(self)
        self.progress_bar.setRange(0, 100)
        
        #Defining the categories of data that the window will dipslay        
        self.label_temp = QLabel(self)
        self.label_RH = QLabel(self)
        self.label_DP = QLabel(self)
        self.timestamp = QLabel(self)        

        #Adding the seperate sections to display these categories of data
        layout = QVBoxLayout(self)
        layout.addWidget(self.progress_bar)
        layout.addWidget(self.label_temp)        
        layout.addWidget(self.label_RH)
        layout.addWidget(self.label_DP)
        layout.addWidget(self.timestamp)
        
        #Setting the window size to fit the entire screen upon opening
        self.setModal(True)
        self.setMinimumSize(800, 500)
        
        #We define a timer on the class. This will enable the window to refresh every 100ms, and show new data that is acquired. If no new data point is acquired, then the window will refresh with the last recorded value.  
        self.timer = QTimer(self)
        self.timer.timeout.connect(self.refresh_data)
        self.timer.start(100) # This refreshes the window every 5 seconds.
        
        self.plot_widget = pg.PlotWidget(title="Climatic Chamber Conditions")
        self.plot_widget.setBackground("w")
        self.plot_widget.setLabel("left", "Temperature", units="C")
        self.plot_widget.setLabel("bottom", "Seconds since start", units="s")

        #Defining the arrays for the data to be collected into. 
        self.time_data = []
        self.temp_data = []
        self.rhum_data = []
        self.dewp_data = []

        #Defining categories of elements that the QT Popup window will account for in the plot.
        self.temperature_plot = self.plot_widget.plot(pen='b',  name='Temperature')
        self.rel_humidity_plot = self.plot_widget.plot(pen='r', name='Rel. Humidity')

        self.dewpoint_plot = self.plot_widget.plot(pen='g', name='Dew Point')

        layout.addWidget(self.plot_widget)
        self.start_time = time.time()
        self.total_elapsed_time = 0
        self.count = -1

    # Defining the function that will execute the commands, ultimatley refreshing the data with new points every time it's called upon. 
    def refresh_data(self):
        #Collecting temperature data from the THSensor Bucket
        query_api = client.query_api()
        query_temp = 'from(bucket:"THSensor")\
                    |>range(start: -160h)\
                    |>filter(fn:(r) => r._measurement == "temperature" and r._field == "sensor_T")'
        temp = query_api.query(org=db_org, query=query_temp)
        
        #Getting a numerical value for the temperature
        for table in temp:
            for record_t in table.records:
                temperature = record_t.get_value()
        
        #Collecting Relative Humidity data from the THSensor Bucket
        query_RH = 'from(bucket:"THSensor")\
                    |>range(start: -160h)\
                    |>filter(fn:(r) => r._measurement == "temperature" and r._field == "sensor_RH")'
        RH = query_api.query(org=db_org, query=query_RH)

        #Getting a numeical value for the Relative Humiditiy.
        for table in RH:
            for record_RH in table.records:
                Rel_Humidity = record_RH.get_value()
        
        #Collecting DewPoint data from the THSensor Bucket
        query_DP = 'from(bucket:"THSensor")\
                    |>range(start: -160h)\
                    |>filter(fn:(r) => r._measurement == "temperature" and r._field == "sensor_DP")'
        DP = query_api.query(org=db_org, query=query_DP)

        #Getting a numerical value for the Dew Point. 
        for table in DP:
            for record_DP in table.records:
                DewPoint = record_DP.get_value()
        now = datetime.now()
        timestamp = now.strftime("Current time: %m-%d %H:%M:%S")

        #Defining the text to put in each data reading display box, and writing in the correct formatting. 
        self.progress_bar.setValue(100)
        self.label_temp.setText("<font color='blue'>---</font> Temperature: %.2f °C" % temperature)
        self.label_temp.setStyleSheet("border: 1px solid black;")
        self.label_temp.setFont(QFont('Arial', 16))

        self.label_RH.setText("<font color='red'>---</font> Rel. Humidity: %.2f %%" % Rel_Humidity)
        self.label_RH.setStyleSheet("border: 1px solid black;")
        self.label_RH.setFont(QFont('Arial', 16))

        self.label_DP.setText("<font color='green'>---</font> DewPoint: %.2f °C" % DewPoint)
        self.label_DP.setStyleSheet("border: 1px solid black;")
        self.label_DP.setFont(QFont('Arial', 16))
        
        self.timestamp.setText(timestamp)
        self.timestamp.setStyleSheet("border: 1px solid black;")
        self.timestamp.setFont(QFont('Arial', 16))

        #We define the time as the current time. Then in order to get the time in terms of seconds passed since the start of the collection, we subtract the start time from the current time. 
        now = time.time()
        elapsed_seconds = int(now - self.start_time)
        
        #This if statement allows the x-axis of the pop up plot to refresh every120 seconds. Once 120 seconds is reached in the data collection, the x axis will shift to the next 120 seconds. 
        if elapsed_seconds > (self.count*120):
           # print(self.count)
            x_range_min = (self.count*120)
            x_range_max = (x_range_min + 120)
            self.plot_widget.setXRange(x_range_min, x_range_max)
            self.count = self.count + 1

        self.time_data.append(elapsed_seconds)
        self.temp_data.append(temperature)
        self.rhum_data.append(Rel_Humidity)
        self.dewp_data.append(DewPoint)


        self.temperature_plot.setData(self.time_data, self.temp_data)
        self.rel_humidity_plot.setData(self.time_data, self.rhum_data)
        self.dewpoint_plot.setData(self.time_data, self.dewp_data)
        
        self.plot_widget.update()

#Final Commands to execute the entire class and its defined functions. 
app = QApplication(sys.argv)
progress_dialog = ProgressDialog()
progress_dialog.show()
sys.exit(app.exec_())

